Pod::Spec.new do |s|
    s.name             = 'vattanac-pay-sdk'
    s.version          = '1.0.0'
    s.summary          = 'Payment Gateway SDK..'
    s.description      = <<-DESC
    Introducing Payment Gateway SDK. 
    Now all online merchants can integrate Vattanac Bank Payment Gateway to existing and newly online store. 
    The payment process is simple, quick and secure. Customer can get a better experience during checkout the payment.
    DESC

    s.homepage         = 'https://www.vattanacbank.com'
    s.license          = { :type => 'Apache', :file => 'LICENSE' }
    s.author           = { 'Vattanac Bank' => 'cphana.vb@gmail.com' }
    s.source           = { :git => 'https://bitbucket.org/vattanacbank/vattanac-bank-payment-gateway-pod.git', :tag => s.version.to_s }
    s.ios.deployment_target = '10.0'
    s.swift_version = '5.0'
    s.ios.vendored_frameworks = 'VattanacPaySDK.xcframework'
    s.user_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'i386' }
    s.pod_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'i386' }
  end
  